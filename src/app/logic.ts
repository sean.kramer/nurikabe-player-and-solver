import { Board } from "./board";
import { Island } from "./island";
import { Mark, Tile } from "./tile";
import { Vec } from "./vec";

export class Logic {

	public static OCTO = [
		new Vec(0, -1),
		new Vec(1, -1),
		new Vec(1, 0),
		new Vec(1, 1),
		new Vec(0, 1),
		new Vec(-1, 1),
		new Vec(-1, 0),
		new Vec(-1, -1),
	];

	constructor(public board: Board) {}

	public unreachable(onlyOne = false): boolean {
		let found = false;
		for (const tile of this.board.flat) {
			if (tile.mark === Mark.MYST && !Object.keys(tile.reachableBy).length) {
				tile.mark = Mark.SEA;
				found = true;
				if (onlyOne) { break; }
			}
		}
		this.board.reach();
		return found;
	}

	public forced(onlyOne = false, onlyThis?: Island): boolean {
		let found = false;
		for (const island of this.board.islands) {
			if (island.complete) { continue; }
			if (onlyThis && island !== onlyThis) { continue; }
			const potentials = island.potentials;
			for (const check of potentials) {
				if (check.orphan) { continue; }
				const revert = check.mark;
				// Run reach with the orphan check
				check.mark = Mark.SEA;
				if (!island.reach(false)) {
					found = true;
					island.add(check);
					if (onlyOne) { break; }
				} else {
					check.mark = revert;
				}
			}
			if (found && onlyOne) { break; }
		}
		this.board.reach();
		return found;
	}

	public fourSquare(onlyOne = false): boolean {
		let found = false;
		for (const tile of this.board.flat) {
			if (tile.mark !== Mark.MYST) { continue; }
			for (let i = 0; i < 8; i += 2) {
				let count = 0;
				for (let j = i; j < i + 3; j++) {
					const test = this.board.get(Logic.OCTO[j % 8].add(tile.x, tile.y));
					if (!test || test.mark !== Mark.SEA) { break; }
					count++;
				}

				// Tile is surrounded by 3 sea tiles
				if (count === 3) {
					tile.mark = Mark.LAND;
					found = true;
					const values = Object.values(tile.reachableBy);
					if (values.length === 1) {
						values[0].add(tile);
						this.board.reach();
					}
					break;
				}
			}
			if (found && onlyOne) { break; }
		}
		return found;
	}

	public connected(onlyOne = false): boolean {
		let found = false;
		let baseline: number;
		for (const tile of this.board.flat) {
			if (tile.mark !== Mark.MYST) { continue; }
			let sea: Tile;
			for (const vec of tile.borders()) {
				const check = this.board.get(vec);
				if (check && check.mark === Mark.SEA) {
					sea = check;
					break;
				}
			}
			if (!sea) { continue; }
			if (baseline === undefined) {
				baseline = this.countSpread(sea, tile);
				this.board.flat.forEach(t => delete t.myst);
			}

			tile.mark = Mark.LAND;
			if (this.countSpread(sea, tile) !== baseline - 1) {
				tile.mark = Mark.SEA;
				found = true;
				if (onlyOne) { break; }
			} else {
				tile.mark = Mark.MYST;
			}
			
		}
		
		// Clear the myst marker in case some changes to the board
		// cause the last checked tile to be the first checked next time
		// (would cause the DFS to fail)
		this.board.flat.forEach(t => delete t.myst);

		// If any new sea tiles were marked, recalc the island's reach
		if (found) { this.board.reach(); }
		return found;
	}

	/**
	 * DFS across the whole board to see how many myst/sea tiles
	 * can be reached from the given position
	 */
	private countSpread(check: Tile, mystMarker: Tile): number {
		let count = 1;
		check.myst = mystMarker;
		for (const vec of check.borders()) {
			const tile = this.board.get(vec);
			if (tile && tile.mark !== Mark.LAND && tile.myst !== mystMarker) {
				count += this.countSpread(tile, mystMarker);
			}
		}
		return count;
	}

}
