import { Tile, Pause } from "./tile";
import { Board } from "./board";

export class Puzzle {

	private counter = 0;
	private resolve: () => void;
	public board: Board;

	constructor() {
		// this.board = new Board(9);
	}

	next(count = 1) {
		this.counter = count;
		if (this.resolve) {
			this.counter--;
			this.resolve();
			delete this.resolve;
		}
	}

	async pause() {
		return new Promise<void>(resolve => {
			if (this.counter > 0) {
				this.counter--;
				resolve();
			} else {
				this.resolve = resolve;
			}
		});
	}

	public solve() { return new Promise(resolve => { this._solve(); resolve(); }); }

	async _solve() {
		const p: Pause = this.pause.bind(this);


		while (true) {
			// let changed = false;
			// changed = await g.laser(p) || changed;

			await this.pause();
			console.log("FINISH");
		}
	}

}
