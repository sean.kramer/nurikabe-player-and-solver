import { Island } from "./island";
import { Vec } from "./vec";

export type Pause = () => Promise<void>;

export enum Mark { LAND, SEA, MYST }

export class Tile {

	public mark = Mark.MYST;

	public reachableBy: {[key: string]: Island} = {};
	public island?: Island;
	public orphan?: boolean;
	public myst?: Tile;

	constructor(
		public x: number,
		public y: number,
		public og?: number,
	) {
		if (og) {
			this.mark = Mark.LAND;
		}
	}

	set(val: number, max?: number): boolean {
		if (val < 0) { return false; }
		if (val === 0) {
			const change = this.mark !== Mark.MYST;
			this.mark = Mark.MYST;
			delete this.og;
			return change;
		}
		if (!this.og) {
			this.mark = Mark.LAND;
			this.og = val;
		} else {
			this.og = this.og * 10 + val;
		}
		return true;
	}

	cycle(): boolean {
		if (this.og) { return false; }
		if (this.mark === Mark.MYST) {
			this.mark = Mark.SEA;
		} else if (this.mark === Mark.SEA) {
			this.mark = Mark.LAND;
		} else {
			this.mark = Mark.MYST;
		}
		return true;
	}

	clear() {
		this.reachableBy = {};
		delete this.island;
		delete this.orphan;
		delete this.myst;
	}

	borders(): Vec[] {
		return Vec.borders(this.x, this.y);
	}

}
