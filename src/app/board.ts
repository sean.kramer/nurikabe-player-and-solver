import { Island } from "./island";
import { Mark, Tile } from "./tile";
import { Vec } from "./vec";

export type Coord = {x: number, y: number};

export class Board {

	public tiles: Tile[][] = [];
	public flat: Tile[] = [];

	public islands!: Island[];

	public x: number;
	public y: number;

	public boundGet: (vec: Vec) => Tile = this.get.bind(this);

	constructor(seed: string) {
		const rle = seed.trim().split(",");
		this.x = +rle[0];
		
		let row = [];
		let x = 0;
		let y = 0;

		const add = (val?: number) => {
			const tile = new Tile(x, y, val);
			row.push(tile);
			this.flat.push(tile);

			if (row.length === this.x) {
				this.tiles.push(row);
				row = [];
				y++;
				x = 0;
			} else {
				x++;
			}
		};

		for (let step = 1; step < rle.length; step++) {
			const val = +rle[step];
			if (step % 2) {
				// Run length of zeros
				for (let i = 0; i < val; i++) {
					add();
				}
			} else {
				// Raw value
				add(val);
			}
		}

		this.y = this.tiles.length;
		this.analyze();
	}

	static empty(x: number, y = x) {
		return new Board(x + "," + x * y);
	}

	async analyze() {
		// Clear islands and tiles of previous data
		this.islands = [];
		this.flat.forEach(tile => tile.clear());

		// Create new islands from current layout
		this.flat.forEach(tile => {
			if (!tile.og) { return; }
			// Islands automatically spread to connected land when created
			this.islands.push(new Island(tile, this.boundGet));
		});

		this.reach();

		// Assign known orphan tiles to their appropriate island
		this.flat.forEach(tile => {
			if (tile.mark !== Mark.LAND || tile.island) { return; }
			const islands = Object.values(tile.reachableBy);
			if (islands.length === 1) {
				islands[0].add(tile);
			}
		});
	}

	public reach() {
		// Calculate potential island reaches
		this.islands.forEach(island => {
			if (!island.reach()) {
				island.impossible = true;
			}
		});
	}

	public encode(): string {
		let build = this.x + ",";
		let count = 0;
		for (const s of this.flat) {
			if (s.og) {
				build += count + "," + s.og + ",";
				count = 0;
			} else {
				count++;
			}
		}
		build += count;
		return build;
	}

	public get(vec: Vec): Tile | undefined {
		if (vec.x < 0 || vec.x >= this.x || vec.y < 0 || vec.y >= this.y) { return; }
		return this.tiles[vec.y][vec.x];
	}

}
