import { Component } from "@angular/core";
import { Board } from "./board";
import { Logic } from "./logic";
import { Mark } from "./tile";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {

	// public puzzle: Puzzle;
	public board: Board;
	public logic: Logic;
	public hx = 0;
	public hy = 0;

	// tslint:disable-next-line:max-line-length
	// public seed = "15,3,1,19,1,1,1,7,1,3,1,3,9,8,4,7,5,1,8,20,2,6,1,3,1,6,1,1,1,2,1,1,1,2,9,5,10,2,1,1,1,2,1,8,1,3,9,6,4,20,2,1,4,7,4,8,1,3,1,3,1,7,1,1,1,19,1,3";
	// public seed = "9,16,3,7,7,6,3,4,6,7,3,4,4,6,5,7,1,16";
	public seed = "9,2,4,10,6,4,7,2,2,9,3,17,4,9,1,2,4,4,2,10,3,2";
	// public seed = "9,8,5,0,1,18,4,2,2,1,3,13,7,1,3,2,2,18,1,0,4,8";
	// public seed = "9,1,7,17,1,3,2,3,2,11,8,1,1,11,6,3,1,3,4,17,6,1";

	public MYST = Mark.MYST;
	public LAND = Mark.LAND;
	public SEA = Mark.SEA;

	constructor() {
		// this.board = Board.empty(10);
		this.board = new Board(this.seed);
		this.logic = new Logic(this.board);

		window.onkeydown = (event) => {
			switch (event.key) {
			case "ArrowUp": case "W": case "w":
				this.hy -= 1;
				if (this.hy < 0) {
					this.hy += this.board.y;
				}
				break;
			case "ArrowDown": case "S": case "s":
				this.hy += 1;
				if (this.hy >= this.board.y) {
					this.hy -= this.board.y;
				}
				break;
			case "ArrowLeft": case "A": case "a":
				this.hx -= 1;
				if (this.hx < 0) {
					this.hx += this.board.x;
				}
				break;
			case "ArrowRight": case "D": case "d":
				this.hx += 1;
				if (this.hx >= this.board.x) {
					this.hx -= this.board.x;
				}
				break;
			case " ": case "Enter": case "Return":
				this.cycle();
				break;
			case "Delete": case "Backspace":
				this.set(0);
				break;
			default:
				if (!isNaN(event.key as any)) {
					this.set(+event.key);
				}
			}
		};
	}

	solve(onlyOne?: boolean) {
		let found: boolean;
		do {
			found = false;
			if (this.logic.unreachable(onlyOne)) {
				if (onlyOne) { break; }
				found = true;
			}
			if (this.logic.fourSquare(onlyOne)) {
				if (onlyOne) { break; }
				found = true;
			}
			if (this.logic.forced(onlyOne)) {
				if (onlyOne) { break; }
				found = true;
			}
			if (this.logic.connected(onlyOne)) {
				if (onlyOne) { break; }
				found = true;
			}
		} while (found);
	}

	load() {
		const level = prompt("Puzzle Code:");
		this.board = new Board(level);
		this.logic = new Logic(this.board);
	}

	highlight(x: number, y: number) {
		this.hx = x;
		this.hy = y;
	}

	cycle(x = this.hx, y = this.hy) {
		if (this.board.tiles[y][x].cycle()) {
			this.board.analyze();
		}
		return false;
	}

	set(val: number) {
		if (this.board.tiles[this.hy][this.hx].set(val)) {
			this.board.analyze();
		}
	}

	print() {
		const code = this.board.encode();
		alert("Level code has been copied to your clipboard.");
		navigator.clipboard.writeText(code);
		console.log(code);
	}

	clear() {
		this.board = Board.empty(this.board.x, this.board.y);
		this.logic = new Logic(this.board);
	}

	reset() {
		this.board = new Board(this.board.encode());
		this.logic = new Logic(this.board);
	}

}
