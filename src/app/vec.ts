export class Vec {

	static borders(x: number, y: number) {
		return [
			new Vec(x, y - 1),
			new Vec(x, y + 1),
			new Vec(x - 1, y),
			new Vec(x + 1, y),
		];
	}

	constructor(
		public x: number,
		public y: number
	) {

	}

	add(x: number, y: number) {
		return new Vec(this.x + x, this.y + y);
	}

}
