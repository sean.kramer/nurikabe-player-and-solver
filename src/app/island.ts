import { Mark, Tile } from "./tile";
import { Vec } from "./vec";

export class Island {

	static ID = 0;

	public id = "" + Island.ID++;
	public size: number;
	public impossible = false;
	public complete = false;
	
	private tiles: Tile[] = [];
	private orphans: Tile[] = [];
	
	public potentials: Tile[] = [];
	
	constructor(
		public root: Tile,
		public get: (vec: Vec) => Tile,
	) {
		this.size = root.og;
		this.spread();
	}

	private spread(check = this.root) {
		this.add(check);
		check.borders().forEach(vec => {
			const tile = this.get(vec);
			if (tile && tile.mark === Mark.LAND && !tile.island) {
				this.spread(tile);
			}
		});
	}

	public reach(skipOrphanCheck = true): boolean {
		this.deactivate();

		let ring = this.tiles.slice(0);
		let nextRing: Tile[] = [];
		
		// How far out we can search from any given tile in the island
		let remaining = this.size - this.tiles.length;
		const requiredPotentials = remaining;

		let orphansReached = 0;

		// BFS
		while (ring.length && remaining > 0) {
			for (const tile of ring) {
				const candidates = tile.borders();
				for (const coord of candidates) {
					const candidate = this.get(coord);

					// Make sure the candidate is valid and unvisited
					if (!candidate
						|| candidate.mark === Mark.SEA
						|| (candidate.mark === Mark.LAND && candidate.island && !candidate.orphan)
						|| candidate.reachableBy[this.id]
					) {
						continue;
					}

					// Make sure the candidate isn't touching any other islands
					let touching = false;
					for (const vec of candidate.borders()) {
						const check = this.get(vec);
						// `Island !== this` allows searching through orphans
						if (check && check.island && check.island !== this) {
							touching = true;
							break;
						}
					}
					if (touching) { continue; }

					// Set the candidate as reachable
					candidate.reachableBy[this.id] = this;
					this.potentials.push(candidate);

					// And then add it to the next round of searches
					nextRing.push(candidate);

					// If it was an orphan belonging to this island, consider it reached
					if (candidate.orphan && candidate.island === this) {
						orphansReached++;
					}
				}
			}

			ring = nextRing;
			nextRing = [];
			remaining--;
		}

		if (skipOrphanCheck && this.orphans.length) {
			this.orphanFilter(requiredPotentials - this.orphans.length);
		}
		
		return (skipOrphanCheck || orphansReached === this.orphans.length) && this.potentials.length >= requiredPotentials;
	}

	/**
	 * @returns true if the tile was added to the main bulk of the island,
	 * false if oprhaned
	 */
	public add(tile: Tile): boolean {
		tile.mark = Mark.LAND;
		tile.island = this;
		
		if (tile !== this.root && !this.connected(tile)) {
			this.orphan(tile);
			return false;
		}

		if (tile.orphan) {
			this.orphans.splice(this.orphans.indexOf(tile), 1);
			tile.orphan = false;
		}
		
		tile.reachableBy = {};
		this.tiles.push(tile);
		this.join(tile);

		if (this.tiles.length === this.size) {
			this.deactivate();
			this.complete = true;
		} else {
			this.integrate();
		}
		this.legal();
	}

	private join(tile: Tile) {
		tile.borders().forEach(vec => {
			const check = this.get(vec);
			if (check && check.mark === Mark.LAND && !check.island) {
				this.add(check);
			}
		});
	}

	private deactivate() {
		this.potentials.forEach(tile => delete tile.reachableBy[this.id]);
		this.potentials = [];
	}

	private orphan(tile: Tile) {
		tile.orphan = true;
		this.orphans.push(tile);
		this.join(tile);
		this.legal();
	}

	private orphanFilter(remaining: number) {
		const reached: Tile[] = this.orphans.slice(0);
		let ring = this.orphans;
		let nextRing: typeof ring = [];
		while (ring.length && remaining > 0) {
			for (const tile of ring) {
				for (const vec of tile.borders()) {
					const check = this.get(vec);
					if (!check || !check.reachableBy[this.id]) { continue; }
					reached.push(check);
					nextRing.push(check);
					delete check.reachableBy[this.id];
				}
			}
			ring = nextRing;
			nextRing = [];
			remaining--;
		}
		this.deactivate();
		reached.forEach(tile => {
			this.potentials.push(tile);
			tile.reachableBy[this.id] = this;
		});
	}

	private connected(tile: Tile): boolean {
		for (const vec of tile.borders()) {
			const check = this.get(vec);
			if (check && check.island === this && !check.orphan) {
				return true;
			}
		}
		return false;
	}

	private integrate() {
		for (const orphan of this.orphans) {
			if (this.connected(orphan)) {
				this.add(orphan);
				// Stop iterating because integrate will be called again anyway
				// when the add funciton succeeds
				return;
			}
		}
	}

	private legal() {
		if (this.tiles.length + this.orphans.length > this.size) {
			this.impossible = true;
			console.log(this.tiles.length + this.orphans.length);
		}
	}

}
